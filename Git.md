## GIT POLICY
### Branch naming:
- `prod`, `release/x.x.x`, `stag`, `pre-stag`, `test`, `dev`, `feat/[tag-name]`, `bugfix/[tag-name]`, `feat/[tag-name]-stag`, `bugfix/[tag-name]-stag`, `hotfix/[tag-name]`
- `prod`: for production environment
- `release/x.x.x`: for release version
- `stag`: for staging environment
- `pre-stag`: prepare features for deployment in the `stag`
- `test`: for testing environment. Maybe called `dev` in some cases
- `dev`: whole code of project, prepare features for deployment in the `test`. Maybe called `pre-dev` in some cases
- `feat/[tag-name]` eg: `feat/auth`, `feat/user-management`, `feat/JIRA-TASK-ID`, `feat/[username]` ... for specific feature. If we have any different setting for other environment, add more suffix like that `feat/auth-stag`, `feat/[username]-stag`
- `bugfix/[tag-name]`: for fix bugs
- `hotfix/[tag-name]`: for hotfix on production enviroment (from production branch). After hotfix done, all of branches must be rebased this branch to get updated code.

**The number of branches depend on the specific project*

### Git flow
- Start a feature: `[dev] $ git checkout -b feat/new-feature`
- Create a pull request to finish a feature

### Update latest code from remote:

1. If remote feature branch has update and local feature branch has no update:

    `[feature] $ git pull origin/feature`
    ```
    [feat/auth] $ git pull origin/feat/auth
    ```

2. If both remote feature branch and local feature branch have new update:

    `[feature] $ git stash`
    `[feature] $ git rebase origin/feature`
    ```
    [feat/auth] $ git stash
    [feat/auth] $ git rebase origin/feat/auth
    ```

3. or if ensure code would be committed

    `[feature] $ git commit -m "." `
    `[feature] $ git rebase origin/feature`
    ```
    [feat/auth] $ git commit -m "specific message"
    [feat/auth] $ git rebase origin/feat/auth
    ```

4. Do the same steps for develop branch
### Update code from parent branch(nearest parent)
1. After commit to parent branch, only need to rebase on feature branch:

    `[feature] $ git rebase dev`
    ```
    [feat/auth] $ git rebase dev
    ```

2. Same steps for Develop branch when master has new update
### Danger!!! Resolve conflict during rebase
1. Merge code manually

    `[feature] $ git add <change-file>`
    ```
    [feat/auth] $ git add <change-file>
    ```
2. Continue rebasing when you have new file

    `[feature] $ git rebase --continue`
    ```
    [feat/auth] $ git rebase --continue
    ```
3. Continue rebasing when you don’t have new file

    `[feature] $ git rebase --skip`
    ```
    [feat/auth] $ git rebase --skip
    ```
4. Cancel rebase

    `[feature] $ git rebase --abort`
    ```
    [feat/auth] $ git rebase --abort
    ```

### Push code to remote
1. Check other commits

    `[feature] $ git reabase origin/feature`
    ```
    [feat/auth] $ git reabase origin/feat/auth
    ```
2. User push command

    `[feature] $ git push origin/feature`
    ```
    [feat/auth] $ git push origin/feat/auth
    ```
3. **Danger!!!** In case of changing history on remote, add force option(need to confirm with TL/PM before run command)

    `[feature] $ git push --force origin/feature`
    ```
    [feat/auth] $ git push --force origin/feat/auth
    ```

### Note
1. Commit format: 

    `<Type>: <[JIRA_ID]> action description on/for screen/function`
    ```
    <Type>: <[JIRA_ID]> action description on/for screen/function
    ```
    - **Type** (required): A type of change that you're committing. eg: feat, fix, docs, style, refactor,...
    - **JIRA_ID** (optional): prefer to add, if not please create jira issue for this action
2. In case of processing commits separately, you can create a local branch then delete it after finished
3. Remove changes on local branch

    `[feature] $ git reset --hard origin/feature`
    ```
    [feat/auth] $ git reset --hard origin/feat/auth
    ```

### Use SSH (REQUIRED)
1. [Create ssh key](https://docs.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key)
@@ -70,12 +114,13 @@

2. NPM package

    `yarn add -D commitizen`

    `./node_modules/commitizen/bin/commitizen init huyhq-cz-jira-smart-commit --yarn --dev --exact`
    ```
    $ yarn add -D commitizen
    $ ./node_modules/bin/commitizen init huyhq-cz-jira-smart-commit --yarn --dev --exact
    ```

    **package.json**
    ```
    ```json
    {
        ...
        "scripts": {
@@ -86,7 +131,9 @@
    ```
    **Use**

    `$ yarn commit`
    ```
    $ yarn commit
    ```

    ![](https://i.imgur.com/lZJLOaa.png)